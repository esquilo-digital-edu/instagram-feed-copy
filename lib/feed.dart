import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class Feed extends StatefulWidget {
  @override
  _FeedState createState() => _FeedState();
}

class _FeedState extends State<Feed> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: titleBar(),
      body: SingleChildScrollView(
        child: Column(
          children: [containerStories(), itemPost()],
        ),
      ),
    );
  }

  Widget itemPost() {
    return Column(
      children: [
        // cabeçalho
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            children: [
              // foto de perfil
              Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(50),
                  border: Border.all(
                    width: 3,
                    color: Colors.black,
                  ),
                  color: Colors.black,
                ),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(25),
                  child: Image.network(
                    'https://yt3.ggpht.com/a-/AOh14GiDIH9wkyHVNLw-r-FLjSxix1HHyAK5a34-95Ag4Q=s88-c-k-c0xffffffff-no-rj-mo',
                    width: 45,
                  ),
                ),
              ),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Text(
                        'Lucas Firmino',
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                      Text('Canoas, RS'),
                    ],
                  ),
                ),
              ),

              IconButton(
                onPressed: () {},
                icon: Icon(Icons.more_horiz),
              )
            ],
          ),
        ),
        // imagem do post
        Image.network(
          'https://png.pngtree.com/png-clipart/20190520/original/pngtree-social-media-background-icons-instagram-emoji-emojis-post-template-png-image_4138570.jpg',
          width: MediaQuery.of(context).size.width,
          height: 300,
          fit: BoxFit.cover,
        ),
        // rodapé
        Row(
          children: [
            Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 4.0, vertical: 8),
                child: Image.asset(
                  'assets/images/like.jpeg',
                  width: 30,
                )),
            Padding(
                padding: const EdgeInsets.symmetric(horizontal: 4.0),
                child: Image.asset(
                  'assets/images/comments.jpeg',
                  width: 30,
                )),
            Padding(
                padding: const EdgeInsets.symmetric(horizontal: 4.0),
                child: Image.asset(
                  'assets/images/direct.jpeg',
                  width: 30,
                )),
            Spacer(),
            Padding(
                padding: const EdgeInsets.symmetric(horizontal: 4.0),
                child: Image.asset(
                  'assets/images/maker.jpeg',
                  width: 30,
                )),
          ],
        ),

        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              RichText(
                text: TextSpan(
                  children: [
                    TextSpan(
                      text: 'Lucas Firmino',
                      style: TextStyle(
                        color: Colors.black, fontWeight: FontWeight.bold
                      ),
                    ),
                    TextSpan(
                      text: ' Texto da minha potagem',
                      style: TextStyle(
                        color: Colors.black,
                      ),
                    )
                  ],
                ),
              )
            ],
          ),
        )
      ],
    );
  }

  Widget containerStories() {
    return Padding(
      padding: EdgeInsets.all(10),
      child: SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        child: Row(
          children: [
            itemStorie(),
            itemStorie(),
            itemStorie(),
            itemStorie(),
            itemStorie(),
            itemStorie(),
            itemStorie(),
          ],
        ),
      ),
    );
  }

  Widget itemStorie() {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 5, horizontal: 3),
      child: Column(
        children: [
          Container(
            width: 65,
            height: 65,
            margin: EdgeInsets.only(bottom: 5),
            decoration: BoxDecoration(
              color: Colors.red,
              borderRadius: BorderRadius.circular(50),
              border: Border.all(color: Colors.blueGrey, width: 3),
            ),
            child: Container(
              width: 65,
              height: 65,
              decoration: BoxDecoration(
                image: DecorationImage(
                    image: NetworkImage(
                        'https://previews.123rf.com/images/defmorph/defmorph1903/defmorph190300032/121047765-social-media-icon-comment-label-gradient-color-instagram-insta-comment-button-symbol-ui-sign-logo-me.jpg')),
                borderRadius: BorderRadius.circular(50),
                border: Border.all(color: Colors.white, width: 3),
              ),
            ),
          ),
          Text('lucas')
        ],
      ),
    );
  }

  Widget titleBar() {
    return AppBar(
      elevation: 1,
      title: Text(
        'Instagram',
        style: GoogleFonts.pacifico(color: Colors.black, fontSize: 28),
      ),
      centerTitle: true,
      backgroundColor: Colors.white,
      actions: [
        Image.asset(
          'assets/images/direct.jpeg',
          width: 35,
        )
      ],
      leading: Icon(
        Icons.photo_camera,
        color: Colors.black,
      ),
    );
  }
}
